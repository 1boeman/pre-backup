#!/bin/bash

# doing the sstuff before backing the stuff up

TAR_DIR=''
TAR_DEST='/tmp/systembackup.tar.gz'
DUMP_DB=''
MYSQL_FILE=''
cd "$(dirname "$0")"
. ./config.sh

if [ ${#MYSQL_FILE} -ge 1 ]; then
    mysqldump --all-databases --user="root" --result-file=$MYSQL_FILE
fi

if [ ${#TAR_DIR} -ge 1 ]; then
    tar -zcf $TAR_DEST $TAR_DIR
fi

